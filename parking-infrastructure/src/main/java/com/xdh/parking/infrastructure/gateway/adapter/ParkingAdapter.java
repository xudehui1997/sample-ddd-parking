package com.xdh.parking.infrastructure.gateway.adapter;

import com.xdh.parking.domain.adapter.IParkingAdapter;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * IParkingAdapter 实现
 *
 * @author coderxdh
 * @date 2024/4/6 21:49
 */
@Service
public class ParkingAdapter implements IParkingAdapter {

    // 使用 api 层提供的外部服务能力

    @Override
    public void sendWxMessage(List<String> receiverList, String content) {
        // 使用 api 层提供的外部服务能力
    }

    @Override
    public void sendEmail(List<String> receiverList, String content) {
        // 使用 api 层提供的外部服务能力
    }
}
