package com.xdh.parking.infrastructure.persistent.dao;

import com.xdh.parking.infrastructure.persistent.po.ParkingPO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IParkingDao {

    void insert(ParkingPO parkingPO);

    ParkingPO selectByPlate(String plate);

}
