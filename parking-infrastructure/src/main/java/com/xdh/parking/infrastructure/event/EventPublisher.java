package com.xdh.parking.infrastructure.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 消息发送
 *
 * @author: xudehui1
 * @date: 2024-04-06 17:41
 */
@Slf4j
@Component
public class EventPublisher {

    public void publish(String topic, String eventMessageJSON) {
        // 使用 RabbitMQ 或者 RocketMQ 发送消息
    }
}
