package com.xdh.parking.infrastructure.persistent.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author: xudehui1
 * @date: 2024-04-05 13:48
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ParkingPO {

    private Long id;

    private String plate;

    private LocalDateTime checkInTime;

    private LocalDateTime lastPlayTime;

    private Integer totalPaid;
}
