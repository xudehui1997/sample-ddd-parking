package com.xdh.parking.infrastructure.persistent.repository;

import com.xdh.parking.domain.model.aggregate.Parking;
import com.xdh.parking.domain.model.valobj.Plate;
import com.xdh.parking.domain.repository.IParkingRepository;
import com.xdh.parking.infrastructure.persistent.dao.IParkingDao;
import com.xdh.parking.infrastructure.persistent.po.ParkingPO;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author: xudehui1
 * @date: 2024-04-05 15:48
 */
@Repository
public class ParkingRepository implements IParkingRepository {

    @Resource
    private IParkingDao parkingDao;

    @Override
    public Parking findByIdOrError(Plate plate) {
        ParkingPO parkingPO = parkingDao.selectByPlate(plate.getValue());
        if (Objects.isNull(parkingPO)) {
            return Parking.builder()
                    .plate(plate)
                    .checkInTime(null)
                    .build();
        }
        return Parking.builder()
                .id(parkingPO.getId())
                .plate(new Plate(parkingPO.getPlate()))
                .checkInTime(parkingPO.getCheckInTime())
                .lastPlayTime(parkingPO.getLastPlayTime())
                .totalPaid(parkingPO.getTotalPaid())
                .build();

    }

    @Override
    public void save(Parking parking) {

        ParkingPO parkingPO = ParkingPO.builder()
                .plate(parking.getPlate().getValue())
                .checkInTime(parking.getCheckInTime())
                .lastPlayTime(parking.getLastPlayTime())
                .totalPaid(parking.getTotalPaid())
                .build();
        parkingDao.insert(parkingPO);
    }
}
