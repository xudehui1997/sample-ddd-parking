package com.xdh.parking.api.req;

import lombok.Data;

/**
 * @author: xudehui1
 * @date: 2024-04-06 11:02
 */
@Data
public class CheckInReq {
    private String plate;

    private String time;
}
