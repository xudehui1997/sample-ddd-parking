package com.xdh.parking.api;

import com.xdh.parking.common.model.Response;

/**
 * 对外 RPC 服务
 *
 * @author coderxdh
 * @date 2024/4/6 20:30
 */
public interface IParkingApi {
    Response<Boolean> doSomeThing();
}
