# DDD开发-停车场案例

## 项目结构说明

### xxx-api:
> 对外提供 RPC 服务

- req：请求入参
- resp：请求出参
- IXxxApi：对外提供的 RPC 服务

### xxx-app
> 应用启动和配置的一层，如一些 aop 切面或者 config 配置，以及打包镜像都是在这一层处理

- config：用于管理引入的Jar所需的资源启动或者初始化处理
- aop：切面相关逻辑
- resources/mybatis：mybatis 配置以及 XxxMapper.xml文件
- resources/xxx：其他配置文件

### xxx-domain
> 领域模型服务，核心模块

- adapter：外部接口适配器层；当需要调用外部接口时，则创建出这一层，并定义接口，之后由基础设施层的 adapter 层具体实现
- command：命令
- event：执行命令过程中出发的事件，但是这个 event 事件发出后的实现逻辑存放的位置该怎么界定呢？
- model：聚合对象，实体对象，值对象
- repository：定义仓储接口，之后由基础设施层做具体实现
- service：领域服务

### xxx-infrastructure
> 基础层依赖于 domain 领域层，遵循依赖倒置

- gateway：接入外部 RPC 服务，实现 domain 领域中的 adapter 接口
  - adapter：实现 domain 领域的 XxxAdapter 接口
  - api：接入外部系统提供的 RPC 服务
  - dto：数据转换
  
- persistent：数据库持久化
  - dao：mapper 层接口
  - po：数据库表对应的实体对象
  - repository：实现 domain 领域的 XxxRepository 接口

- redis: 缓存

### xxx-trigger
> 触发器层，用于提供http接口、消息消费、任务执行等

- api：实现对外提供的 RPC 接口，内部也是调用 domain 领域层的 XxxService 服务，注意事务一致性
- http：提供 HTTP 接口，本质上和 api 层一样
- consumer：消费MQ
- job：定时任务

### xxx-common
> 通用工具、枚举、常量等
