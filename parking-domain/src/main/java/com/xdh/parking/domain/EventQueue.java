package com.xdh.parking.domain;

import java.util.List;

/**
 * @author: xudehui1
 * @date: 2024-04-05 13:54
 */
public interface EventQueue {

    void enqueue(DomainEvent event);

    List<DomainEvent> queue();
}
