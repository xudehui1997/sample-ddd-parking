package com.xdh.parking.domain.service;

import com.xdh.parking.domain.EventQueue;
import com.xdh.parking.domain.command.CalculateFeeCommand;
import com.xdh.parking.domain.command.CheckInCommand;
import com.xdh.parking.domain.command.CheckOutCommand;
import com.xdh.parking.domain.command.PayFeeCommand;
import com.xdh.parking.domain.model.aggregate.Parking;
import com.xdh.parking.domain.repository.IParkingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author: xudehui1
 * @date: 2024-04-05 14:18
 */
@Service
@Slf4j
public class ParkingService {

    @Resource
    private IParkingRepository parkingRepository;

    public Boolean handleCheckIn(EventQueue eventQueue, CheckInCommand command) {
        Parking parking = parkingRepository.findByIdOrError(command.getPlate());
        Boolean result = parking.handle(eventQueue, command);
        parkingRepository.save(parking);
        return result;
    }

    public Integer calculateFeeNow(EventQueue eventQueue, CalculateFeeCommand command) {
        Parking parking = parkingRepository.findByIdOrError(command.getPlate());
        return parking.calculateFeeNow(eventQueue, command.getCalculateTime());
    }

    public Boolean payFee(EventQueue eventQueue, PayFeeCommand command) {
        Parking parking = parkingRepository.findByIdOrError(command.getPlate());
        Boolean result = parking.payFee(eventQueue, command);
        parkingRepository.save(parking);
        return result;
    }

    public Boolean handleCheckOut(EventQueue eventQueue, CheckOutCommand command) {
        Parking parking = parkingRepository.findByIdOrError(command.getPlate());
        Boolean result = parking.handle(eventQueue, command);
        parkingRepository.save(parking);
        return result;
    }
}
