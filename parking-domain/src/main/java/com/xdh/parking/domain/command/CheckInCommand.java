package com.xdh.parking.domain.command;

import com.xdh.parking.domain.model.valobj.Plate;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 车辆进场
 *
 * @author: xudehui1
 * @date: 2024-04-05 14:13
 */
@Data
@AllArgsConstructor
public class CheckInCommand {

    private Plate plate;

    private LocalDateTime checkInTime;
}
