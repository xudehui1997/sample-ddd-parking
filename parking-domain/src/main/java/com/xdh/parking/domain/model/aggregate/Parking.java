package com.xdh.parking.domain.model.aggregate;

import com.xdh.parking.common.exception.BusinessException;
import com.xdh.parking.domain.EventQueue;
import com.xdh.parking.domain.command.CheckInCommand;
import com.xdh.parking.domain.command.CheckOutCommand;
import com.xdh.parking.domain.command.PayFeeCommand;
import com.xdh.parking.domain.event.*;
import com.xdh.parking.domain.model.valobj.Plate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * 停车聚合
 *
 * @author: xudehui1
 * @date: 2024-04-05 13:48
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Parking {

    private Long id;

    private Plate plate;

    private LocalDateTime checkInTime;

    private LocalDateTime lastPlayTime;

    private Integer totalPaid = 0;

    // 停车场信息
    // private ParkingLot parkingLot;

    private static final int FEE_PER_HOUR = 1;


    public Boolean handle(EventQueue eventQueue, CheckInCommand checkInCommand) {
        // 已经进入停车场
        if (inPark()) {
            eventQueue.enqueue(new CheckInFailedEvent(plate, checkInCommand.getCheckInTime(), "车辆已经进入停车场"));
            return false;
        }

        this.checkInTime = checkInCommand.getCheckInTime();
        eventQueue.enqueue(new CheckInSuccessEvent(plate, checkInCommand.getCheckInTime()));
        return true;
    }

    public int calculateFeeNow(EventQueue eventQueue, LocalDateTime now) {
        if (checkInTime == null) {
            throw new BusinessException("车辆尚未入场");
        }

        LocalDateTime currentCheckInTime = checkInTime;
        LocalDateTime lastPayTimeCurrent = lastPlayTime;

        if (lastPayTimeCurrent == null) {
            return feeBetween(currentCheckInTime, now);
        }

        if (totalPaid < feeBetween(currentCheckInTime, lastPayTimeCurrent)) {
            return feeBetween(currentCheckInTime, now) - totalPaid;
        }

        if (lastPayTimeCurrent.plusMinutes(15).isAfter(now)) {
            return 0;
        }

        return feeBetween(currentCheckInTime, now) - totalPaid;
    }

    public Boolean payFee(EventQueue eventQueue, PayFeeCommand command) {
        if (!inPark()) {
            throw new BusinessException("车辆不在场，无需付费");
        }
        lastPlayTime = command.getPayTime();
        totalPaid += command.getAmount();

        eventQueue.enqueue(new PayFeeEvent(plate, command.getAmount(), command.getPayTime()));
        return true;
    }

    public Boolean handle(EventQueue eventQueue, CheckOutCommand checkOutCommand) {
        if (!inPark()) {
            eventQueue.enqueue(new CheckOutFailedEvent(plate, checkOutCommand.getCheckOutTime(), "车辆不在停车场"));
            return false;
        }

        if (calculateFeeNow(eventQueue, LocalDateTime.now()) > 0) {
            throw new BusinessException("缴费未结清");
        }

        this.checkInTime = null;
        this.totalPaid = 0;
        this.lastPlayTime = null;

        eventQueue.enqueue(new CheckOutSuccessEvent(plate, checkOutCommand.getCheckOutTime()));
        return true;
    }

    private int feeBetween(LocalDateTime start, LocalDateTime end) {
        return hoursBetween(start, end) * FEE_PER_HOUR;
    }

    private int hoursBetween(LocalDateTime start, LocalDateTime end) {
        long minutes = Duration.between(start, end).toMinutes();
        long hours = minutes / 60;
        return (int) (hours * 60 == minutes ? hours : hours + 1);
    }

    private Boolean inPark() {
        return checkInTime != null;
    }
}
