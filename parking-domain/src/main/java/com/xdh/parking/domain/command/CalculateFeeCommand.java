package com.xdh.parking.domain.command;

import com.xdh.parking.domain.model.valobj.Plate;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author: xudehui1
 * @date: 2024-04-06 17:09
 */
@Data
@AllArgsConstructor
public class CalculateFeeCommand {
    private Plate plate;

    private LocalDateTime calculateTime;  // 计费时间
}
