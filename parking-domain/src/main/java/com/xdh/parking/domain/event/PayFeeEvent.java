package com.xdh.parking.domain.event;

import com.xdh.parking.domain.DomainEvent;
import com.xdh.parking.domain.model.valobj.Plate;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author: xudehui1
 * @date: 2024-04-05 14:31
 */
@Data
@AllArgsConstructor
public class PayFeeEvent implements DomainEvent {

    private Plate plate;

    private Integer amount;

    private LocalDateTime payTime;
}
