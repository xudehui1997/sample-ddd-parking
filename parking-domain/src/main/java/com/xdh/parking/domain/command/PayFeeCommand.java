package com.xdh.parking.domain.command;

import com.xdh.parking.domain.model.valobj.Plate;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author: xudehui1
 * @date: 2024-04-06 17:19
 */
@Data
@AllArgsConstructor
public class PayFeeCommand {
    private Plate plate;
    private Integer amount;
    private LocalDateTime payTime;
}
