package com.xdh.parking.domain.adapter;

import java.util.List;

/**
 * @author coderxdh
 * @date 2024/4/6 21:43
 */
public interface IParkingAdapter {
    void sendWxMessage(List<String> receiverList, String content);

    void sendEmail(List<String> receiverList, String content);
}
