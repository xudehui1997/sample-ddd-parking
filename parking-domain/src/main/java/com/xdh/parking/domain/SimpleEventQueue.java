package com.xdh.parking.domain;

import java.util.LinkedList;
import java.util.List;

/**
 * @author: xudehui1
 * @date: 2024-04-05 15:24
 */
public class SimpleEventQueue implements EventQueue {
    private List<DomainEvent> queue = new LinkedList<>();

    @Override
    public void enqueue(DomainEvent event) {
        queue.add(event);
    }

    @Override
    public List<DomainEvent> queue() {
        return queue;
    }
}
