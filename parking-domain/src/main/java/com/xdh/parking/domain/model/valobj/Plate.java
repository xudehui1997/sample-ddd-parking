package com.xdh.parking.domain.model.valobj;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Objects;

/**
 * 车辆信息
 *
 * @author: xudehui1
 * @date: 2024-04-05 13:50
 */

@Data
@AllArgsConstructor
public class Plate {
    private final String value;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Plate plate = (Plate) o;
        return Objects.equals(value, plate.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
