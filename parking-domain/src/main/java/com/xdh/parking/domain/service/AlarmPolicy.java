package com.xdh.parking.domain.service;

import com.xdh.parking.domain.DomainEvent;
import com.xdh.parking.domain.DomainEventListener;
import com.xdh.parking.domain.adapter.IParkingAdapter;
import com.xdh.parking.domain.event.CheckInFailedEvent;
import com.xdh.parking.domain.event.CheckInSuccessEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collections;

/**
 * @author: xudehui1
 * @date: 2024-04-06 10:45
 */
@Component
@Slf4j
public class AlarmPolicy implements DomainEventListener {

    @Resource
    private IParkingAdapter parkingAdapter;

    @Override
    public void onEvent(DomainEvent event) {
        if (event instanceof CheckInSuccessEvent) {
            log.info("入场成功");
            parkingAdapter.sendWxMessage(Collections.singletonList("xxx"), "车辆入场成功");
        }
        if (event instanceof CheckInFailedEvent) {
            log.info("入场失败");
            parkingAdapter.sendEmail(Collections.singletonList("xxx"), "车辆入场失败");
        }
    }
}
