package com.xdh.parking.domain;

/**
 * @author: xudehui1
 * @date: 2024-04-05 15:30
 */
public interface DomainEventListener {
    void onEvent(DomainEvent event);
}
