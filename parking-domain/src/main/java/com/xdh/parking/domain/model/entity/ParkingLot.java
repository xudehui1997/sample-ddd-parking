package com.xdh.parking.domain.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 停车场
 *
 * @author: xudehui1
 * @date: 2024-04-05 14:07
 */
@Data
@AllArgsConstructor
public class ParkingLot {
    private Integer totalParkingSpace;

    private Integer remainingParkingSpace;
}
