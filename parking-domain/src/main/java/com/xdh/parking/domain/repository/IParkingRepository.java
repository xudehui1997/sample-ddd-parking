package com.xdh.parking.domain.repository;

import com.xdh.parking.domain.model.aggregate.Parking;
import com.xdh.parking.domain.model.valobj.Plate;

/**
 * @author: xudehui1
 * @date: 2024-04-05 14:18
 */
public interface IParkingRepository {
    Parking findByIdOrError(Plate plate);

    void save(Parking parking);
}
