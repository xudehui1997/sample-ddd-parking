package com.xdh.parking.trigger.invoker;

import com.xdh.parking.domain.EventQueue;

/**
 * @author: xudehui1
 * @date: 2024-04-05 15:31
 */
public interface IDomainEventDispatcher {
    void dispatchNow(EventQueue eventQueue);
}
