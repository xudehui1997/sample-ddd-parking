package com.xdh.parking.trigger.invoker;

import com.xdh.parking.domain.DomainEventListener;
import com.xdh.parking.domain.EventQueue;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: xudehui1
 * @date: 2024-04-05 15:32
 */
@Component
public class SyncIDomainEventDispatcher implements IDomainEventDispatcher {

    @Resource
    private List<DomainEventListener> domainEventListenerList;

    @Override
    public void dispatchNow(EventQueue eventQueue) {
        eventQueue.queue().forEach(event -> {
            domainEventListenerList.forEach(domainEventListener -> {
                domainEventListener.onEvent(event);
            });
        });
    }
}
