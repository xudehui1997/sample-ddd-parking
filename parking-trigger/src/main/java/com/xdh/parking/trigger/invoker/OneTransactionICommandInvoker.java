package com.xdh.parking.trigger.invoker;

import com.xdh.parking.domain.EventQueue;
import com.xdh.parking.domain.SimpleEventQueue;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.function.Function;

/**
 * @author: xudehui1
 * @date: 2024-04-05 15:29
 */
@Component
public class OneTransactionICommandInvoker implements ICommandInvoker {
    private final TransactionTemplate transactionTemplate;
    private final IDomainEventDispatcher domainEventDispatcher;

    public OneTransactionICommandInvoker(PlatformTransactionManager transactionManager,
                                         IDomainEventDispatcher domainEventDispatcher) {
        this.transactionTemplate = new TransactionTemplate(transactionManager);
        this.domainEventDispatcher = domainEventDispatcher;
    }

    @Override
    public <R> R invoke(Function<EventQueue, R> run) {
        return transactionTemplate.execute(status -> {
            SimpleEventQueue eventQueue = new SimpleEventQueue();
            R result = run.apply(eventQueue);
            domainEventDispatcher.dispatchNow(eventQueue);
            return result;
        });
    }
}
