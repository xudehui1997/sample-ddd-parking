package com.xdh.parking.trigger.api;

import com.xdh.parking.api.IParkingApi;
import com.xdh.parking.common.model.Response;
import org.springframework.stereotype.Component;

/**
 * 对外 RPC 服务实现
 *
 * @author coderxdh
 * @date 2024/4/6 20:32
 */
@Component
public class ParkingApi implements IParkingApi {
    @Override
    public Response<Boolean> doSomeThing() {
        // 调用 domain 的 ParkService 服务
        return null;
    }
}
