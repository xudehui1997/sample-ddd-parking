package com.xdh.parking.trigger.http;

import com.xdh.parking.api.req.CheckInReq;
import com.xdh.parking.common.model.Response;
import com.xdh.parking.domain.command.CheckInCommand;
import com.xdh.parking.domain.model.valobj.Plate;
import com.xdh.parking.domain.service.ParkingService;
import com.xdh.parking.trigger.invoker.ICommandInvoker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping(value = "/parking")
public class Controller {

    @Resource
    private ParkingService parkingService;

    @Resource
    private ICommandInvoker commandInvoker;

    /**
     * http://localhost:8090/success
     */
    @PostMapping("/checkIn")
    public Response<Boolean> checkIn(@RequestBody CheckInReq req) {
        log.info("测试调用");
        try {
            Boolean result = commandInvoker.invoke(eventQueue -> parkingService.handleCheckIn(eventQueue, new CheckInCommand(
                    new Plate(req.getPlate()),
                    LocalDateTime.parse(req.getTime())
            )));

            return Response.<Boolean>builder().data(result).build();
        } catch (Exception e) {
            log.error("发生异常：", e);
        }
        return Response.<Boolean>builder().data(false).build();
    }

}
