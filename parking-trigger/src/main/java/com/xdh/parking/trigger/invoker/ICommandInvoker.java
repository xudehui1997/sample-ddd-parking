package com.xdh.parking.trigger.invoker;

import com.xdh.parking.domain.EventQueue;

import java.util.function.Function;

/**
 * @author: xudehui1
 * @date: 2024-04-05 15:27
 */
public interface ICommandInvoker {
    <R> R invoke(Function<EventQueue, R> run);
}
